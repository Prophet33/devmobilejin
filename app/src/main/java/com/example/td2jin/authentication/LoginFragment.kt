package com.example.td2jin.authentication

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.example.td2jin.MainActivity
import com.example.td2jin.R
import com.example.td2jin.network.Api
import com.example.td2jin.tasklist.TaskListFragment
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val loginButton = view.findViewById<Button>(R.id.LP_login_button)
        loginButton.setOnClickListener {
            val emailText = view.findViewById<EditText>(R.id.edit_login_email_text).text.toString()
            val passwordText = view.findViewById<EditText>(R.id.edit_login_password_text).text.toString()
            if (emailText != "" && passwordText != ""){
                val loginForm = LoginForm(emailText, passwordText)
                lifecycleScope.launch {
                    val loginResponse = Api.INSTANCE.userService.login(loginForm)
                    if (loginResponse.isSuccessful) {
                        val fetchedToken = loginResponse.body()!!.token
                        PreferenceManager.getDefaultSharedPreferences(context).edit {
                            putString(SHARED_PREF_TOKEN_KEY, fetchedToken)
                        }
                        val intent = Intent(activity, MainActivity::class.java)
                        startActivityForResult(intent, TaskListFragment.ADD_TASK_REQUEST_CODE)
                    }
                    else {
                        Toast.makeText(context, "Identifiant ou mot de passe incorrect", Toast.LENGTH_LONG).show()
                    }
                }
            }
            else Toast.makeText(context, "Tous les champs doivent être remplis", Toast.LENGTH_LONG).show()

        }
    }
}