package com.example.td2jin.authentication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.td2jin.R
import com.example.td2jin.network.Api
import kotlinx.coroutines.launch

class SignupFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val signupButton = view.findViewById<Button>(R.id.SP_signup_button)
        signupButton.setOnClickListener {
            val firstnameText = view.findViewById<EditText>(R.id.edit_signup_firstname).text.toString()
            val lastnameText = view.findViewById<EditText>(R.id.edit_signup_lastname).text.toString()
            val emailText = view.findViewById<EditText>(R.id.edit_signup_email).text.toString()
            val passwordText = view.findViewById<EditText>(R.id.edit_signup_password).text.toString()
            val confPasswordText = view.findViewById<EditText>(R.id.edit_signup_confirmation_password).text.toString()
            if (firstnameText != "" &&
                lastnameText != "" &&
                emailText != "" &&
                passwordText != "" &&
                confPasswordText != ""){
                val signupForm = SignupForm(firstnameText, lastnameText, emailText, passwordText, confPasswordText)
                lifecycleScope.launch {
                    val signupResponse = Api.INSTANCE.userService.signup(signupForm)
                    if (signupResponse.isSuccessful) {
                        Toast.makeText(context, "Compte créé", Toast.LENGTH_LONG).show()
                        findNavController().navigate(R.id.action_SignupFragment_to_AuthenticationFragment)
                    }
                    else {
                        Toast.makeText(context, "Erreur", Toast.LENGTH_LONG).show()
                    }
                }
            }
            else Toast.makeText(context, "Tous les champs doivent être remplis", Toast.LENGTH_LONG).show()


        }
    }
}