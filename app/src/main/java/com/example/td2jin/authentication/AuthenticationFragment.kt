package com.example.td2jin.authentication

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.td2jin.MainActivity
import com.example.td2jin.R
import com.example.td2jin.network.Api
import com.example.td2jin.task.TaskActivity
import com.example.td2jin.tasklist.TaskListFragment
import com.example.td2jin.userinfo.UserInfoActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textview.MaterialTextView
import kotlinx.coroutines.launch


class AuthenticationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (Api.INSTANCE.getToken() != "") {
            val intent = Intent(activity, MainActivity::class.java)
            startActivityForResult(intent, TaskListFragment.ADD_TASK_REQUEST_CODE)
        }
        return inflater.inflate(R.layout.fragment_authentication, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val loginButton = view.findViewById<Button>(R.id.login_button)
        loginButton.setOnClickListener {
            findNavController().navigate(R.id.action_AuthenticationFragment_to_LoginFragment)
        }

        val signupButton = view.findViewById<Button>(R.id.signup_button)
        signupButton.setOnClickListener {
            findNavController().navigate(R.id.action_AuthenticationFragment_to_SignupFragment)
        }
    }


}