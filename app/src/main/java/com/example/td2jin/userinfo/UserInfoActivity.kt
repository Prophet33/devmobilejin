package com.example.td2jin.userinfo

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.lifecycleScope
import com.example.td2jin.BuildConfig
import com.example.td2jin.R
import com.example.td2jin.network.UserInfo
import com.example.td2jin.network.UserInfoViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class UserInfoActivity : AppCompatActivity() {

    private val viewModel: UserInfoViewModel by viewModels()
    //private val userService = Api.userService


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)

        val pictureButton = findViewById<Button>(R.id.take_picture_button)
        pictureButton.setOnClickListener {
            askCameraPermissionAndOpenCamera()
        }
        val galleryButton = findViewById<Button>(R.id.upload_image_button)
        galleryButton.setOnClickListener {
            pickInGallery.launch("image/*")
        }

        val firstNameText = findViewById<EditText>(R.id.editNameText)
        val nameText = findViewById<EditText>(R.id.editNameText2)
        val emailText = findViewById<EditText>(R.id.editMailText)
        val firstName = viewModel.userInfo.value?.firstName
        val name = viewModel.userInfo.value?.lastName
        val email = viewModel.userInfo.value?.email

        firstNameText.setText(firstName)
        nameText.setText(name)
        emailText.setText(email)

        findViewById<FloatingActionButton>(R.id.confirmEditUserButton).setOnClickListener {
            val newFirstName = firstNameText.text.toString()
            val newName = nameText.text.toString()
            val newEmail = emailText.text.toString()

            viewModel.update(UserInfo(newEmail, newFirstName, newName))
            finish()
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) openCamera()
            else showExplanationDialog()
        }

    private fun requestCameraPermission() =
        requestPermissionLauncher.launch(Manifest.permission.CAMERA)

    private fun askCameraPermissionAndOpenCamera() {
        when {
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED -> openCamera()
            shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) -> showExplanationDialog()
            else -> requestCameraPermission()
        }
    }

    private fun showExplanationDialog() {
        AlertDialog.Builder(this).apply {
            setMessage("On a besoin de la caméra sivouplé ! 🥺")
            setPositiveButton("Bon, ok") { _, _ ->
                requestCameraPermission()
            }
            setCancelable(true)
            show()
        }
    }

    // convert
    private fun convert(uri: Uri) =
            MultipartBody.Part.createFormData(
                    name = "avatar",
                    filename = "temp.jpeg",
                    body = contentResolver.openInputStream(uri)!!.readBytes().toRequestBody()
            )

    private fun handleImage(uri: Uri) {
        lifecycleScope.launch {
            //userService.updateAvatar(convert(uri))
            viewModel.updateAvatar(convert(uri))
        }
    }

    // create a temp file and get a uri for it
    private val photoUri by lazy {
        FileProvider.getUriForFile(
                this,
                BuildConfig.APPLICATION_ID +".fileprovider",
                File.createTempFile("avatar", ".jpeg", externalCacheDir)

        )
    }

    // register
    private val takePicture = registerForActivityResult(ActivityResultContracts.TakePicture()) { success ->
        if (success) handleImage(photoUri)
        else Toast.makeText(this, "Erreur ! 😢", Toast.LENGTH_LONG).show()
    }

    // use
    private fun openCamera() = takePicture.launch(photoUri)

    // register
    private val pickInGallery =
            registerForActivityResult(ActivityResultContracts.GetContent()) { photoUri ->
                handleImage(photoUri)
            }


}