package com.example.td2jin.tasklist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.td2jin.R

//class TaskListAdapter(var taskList: List<Task> = emptyList()) : RecyclerView.Adapter<TaskListAdapter.TaskViewHolder>() {
class TaskListAdapter : ListAdapter<Task, TaskListAdapter.TaskViewHolder>(TaskListDiffCallBack){
    var onDeleteClickListener: ((Task) -> Unit)? = null
    var onEditClickListener: ((Task) -> Unit)? = null

    inner class TaskViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(task: Task) {
            itemView.apply {
                val textView = findViewById<TextView>(R.id.task_title)
                textView.text = task.title
                val textViewDesc = findViewById<TextView>(R.id.task_description)
                textViewDesc.text = task.description

                val deleteButton = findViewById<ImageButton>(R.id.delete_button)
                deleteButton.setOnClickListener{
                    onDeleteClickListener?.invoke(task)
                }

                val editButton = findViewById<ImageButton>(R.id.edit_button)
                editButton.setOnClickListener{
                    onEditClickListener?.invoke(task)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return TaskViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return currentList.size
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(currentList[position])
    }


}

object TaskListDiffCallBack: DiffUtil.ItemCallback<Task>() {

    override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
        return (oldItem.id == newItem.id)
    }

    override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
        return (oldItem.title == newItem.title && oldItem.description == newItem.description)
    }

}
