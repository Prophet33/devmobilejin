package com.example.td2jin.tasklist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.td2jin.R
import com.example.td2jin.authentication.SHARED_PREF_TOKEN_KEY
import com.example.td2jin.network.TaskListViewModel
import com.example.td2jin.network.UserInfoViewModel
import com.example.td2jin.task.TaskActivity
import com.example.td2jin.userinfo.UserInfoActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textview.MaterialTextView
import kotlinx.coroutines.launch

class TaskListFragment : Fragment() {

    companion object {
        const val ADD_TASK_REQUEST_CODE = 333
        const val EDIT_TASK_REQUEST_CODE = 9
    }

    var taskListAdapter = TaskListAdapter()
    private val viewModel: TaskListViewModel by viewModels()
    private val userInfoViewModel: UserInfoViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_task_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = taskListAdapter

        //LiveData
        viewModel.taskList.observe(viewLifecycleOwner) { myList ->
            taskListAdapter.submitList(myList)
        }
        //

        val imageView = view.findViewById<ImageView>(R.id.image_view_avatar)

        userInfoViewModel.userInfo.observe(viewLifecycleOwner) { myUserInfo ->
            val textView = view.findViewById<MaterialTextView>(R.id.text_view)
            if (textView != null) {
                textView.text = "${myUserInfo.firstName} ${myUserInfo.lastName}"
            }
            imageView?.load(myUserInfo.avatar){
                transformations(CircleCropTransformation())
            }
        }

        taskListAdapter.onDeleteClickListener = { task ->
            lifecycleScope.launch {
                viewModel.deleteTask(task)
            }
            taskListAdapter.notifyDataSetChanged()
        }
        taskListAdapter.onEditClickListener = { task ->
            val intent = Intent(activity, TaskActivity::class.java)
            intent.putExtra(TaskActivity.TASK_KEY, task)
            startActivityForResult(intent, EDIT_TASK_REQUEST_CODE)
        }

        val createButton = view.findViewById<FloatingActionButton>(R.id.floating_action_button)
        createButton.setOnClickListener {
            val intent = Intent(activity, TaskActivity::class.java)
            startActivityForResult(intent, ADD_TASK_REQUEST_CODE)
        }

        val signoutButton = view.findViewById<FloatingActionButton>(R.id.signout_button)
        signoutButton.setOnClickListener {
            PreferenceManager.getDefaultSharedPreferences(context).edit {
                putString(SHARED_PREF_TOKEN_KEY, "")
            }
            activity?.finish()
        }

        imageView.setOnClickListener {
            val intent = Intent(activity, UserInfoActivity::class.java)
            startActivityForResult(intent, ADD_TASK_REQUEST_CODE)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && data!=null) {
            if (requestCode == ADD_TASK_REQUEST_CODE) {
                val task = data.getSerializableExtra(TaskActivity.TASK_KEY) as Task
                lifecycleScope.launch {
                    viewModel.addTask(task)
                }
                taskListAdapter.notifyDataSetChanged()
            }
            if (requestCode == EDIT_TASK_REQUEST_CODE) {
                val task = data.getSerializableExtra(TaskActivity.TASK_KEY) as Task
                lifecycleScope.launch {
                    viewModel.editTask(task)
                }
                taskListAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onResume() {
        //LiveData
        viewModel.loadTasks()
        //

        userInfoViewModel.getInfo()
        /*userInfoViewModel.userInfo.observe(viewLifecycleOwner) { myUserInfo ->
            val textView = view?.findViewById<MaterialTextView>(R.id.text_view)
            if (textView != null) {
                textView.text = "${myUserInfo.firstName} ${myUserInfo.lastName}"
            }
            val imageView = view?.findViewById<ImageView>(R.id.image_view_avatar)
            imageView?.load(myUserInfo.avatar){
                transformations(CircleCropTransformation())
            }
        }*/

        /*lifecycleScope.launch {
            val userInfo = Api.userService.getInfo().body()!!
            val textView = view?.findViewById<MaterialTextView>(R.id.text_view)
            if (textView != null) {
                textView.text = "${userInfo.firstName} ${userInfo.lastName}"
            }

            val imageView = view?.findViewById<ImageView>(R.id.image_view_avatar)
            //imageView?.load("https://cdn.discordapp.com/attachments/487006252657016853/790946649760333834/unknown.png") {
            //    transformations(CircleCropTransformation())
            //}
            imageView?.load(userInfo.avatar){
                transformations(CircleCropTransformation())
            }
        }*/

        super.onResume()


    }
}