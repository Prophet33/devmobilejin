package com.example.td2jin.task

import android.app.Activity
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.td2jin.R
import com.example.td2jin.tasklist.Task
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.*

class TaskActivity : AppCompatActivity() {
    companion object {
        const val TASK_KEY = "TASK_KEY"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        var task = intent.getSerializableExtra(TASK_KEY) as? Task
        val oldTitle = task?.title
        val oldDesc = task?.description
        val editText1 = findViewById<EditText>(R.id.editText1)
        val editText2 = findViewById<EditText>(R.id.editText2)
        editText1.setText(oldTitle)
        editText2.setText(oldDesc)

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener {
            var taskTitle = editText1.text.toString()
            if (taskTitle == "") {
                taskTitle = "New Task"
            }
            var taskDescription = editText2.text.toString()
            if (taskDescription == "") {
                taskDescription = "Desc par défaut"
            }
            task = Task(id = task?.id?:UUID.randomUUID().toString(), title = taskTitle, description = taskDescription)
            intent.putExtra(TASK_KEY, task)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}