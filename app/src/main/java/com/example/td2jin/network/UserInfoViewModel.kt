package com.example.td2jin.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

class UserInfoViewModel: ViewModel() {
    private val repository = UserInfoRepository()
    private val _userInfo = MutableLiveData<UserInfo>()
    val userInfo: LiveData<UserInfo> = _userInfo

    fun getInfo() {
        viewModelScope.launch {
            _userInfo.value = repository.getInfo()
        }
    }
    fun updateAvatar(avatar: MultipartBody.Part) {
        viewModelScope.launch {
            _userInfo.value = repository.updateAvatar(avatar)
        }
    }
    fun update(user: UserInfo) {
        viewModelScope.launch {
            _userInfo.value = repository.update(user)
        }
    }
}