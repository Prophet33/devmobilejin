package com.example.td2jin.network
import com.example.td2jin.tasklist.Task

class TasksRepository {
    private val tasksWebService = Api.INSTANCE.taskWebService

    suspend fun loadTasks(): List<Task>? {
        val response = tasksWebService.getTasks()
        return if (response.isSuccessful) response.body() else null
    }

    suspend fun createTask(task: Task){
        tasksWebService.createTask(task)
    }

    suspend fun deleteTask(task: Task){
        tasksWebService.deleteTask(task.id)
    }

    suspend fun updateTask(task: Task) {
        tasksWebService.updateTask(task, task.id)
    }
}