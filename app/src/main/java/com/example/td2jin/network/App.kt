package com.example.td2jin.network

import android.app.Application

class App:Application() {
    override fun onCreate() {
        super.onCreate()
        Api.INSTANCE = Api(this)
    }
}