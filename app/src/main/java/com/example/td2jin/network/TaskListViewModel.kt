package com.example.td2jin.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.td2jin.tasklist.Task
import kotlinx.coroutines.launch

class TaskListViewModel: ViewModel() {

    private val repository = TasksRepository()
    private val _taskList = MutableLiveData<List<Task>>()
    val taskList: LiveData<List<Task>> = _taskList

    fun loadTasks() {
        viewModelScope.launch {
            _taskList.value = repository.loadTasks()
        }
    }
    fun deleteTask(task: Task) {
        viewModelScope.launch {
            repository.deleteTask(task)
            _taskList.value = repository.loadTasks()
        }
    }
    fun addTask(task: Task) {
        viewModelScope.launch {
            repository.createTask(task)
            _taskList.value = repository.loadTasks()
        }
    }
    fun editTask(task: Task) {
        viewModelScope.launch {
            repository.updateTask(task)
            val editableList = _taskList.value.orEmpty().toMutableList()
            val position = editableList.indexOfFirst { task.id == it.id }
            editableList[position] = task
            _taskList.value = editableList
        }
    }
}